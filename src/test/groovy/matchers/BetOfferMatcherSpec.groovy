package matchers

import domain.BetOffer
import domain.BulkedBetOffer
import domain.Outcome
import domain.Probabilities
import events.DomainEventType
import spock.lang.Specification

class BetOfferMatcherSpec extends Specification {
    def matcher = new BetOfferMatcher({ System.currentTimeMillis() })

    def "should detect BetOffer status changes"() {
        given:
        def old = defaultBetOffer()
        def current = defaultBetOffer()
        current.betOffer.status = "ko"

        when:
        def events = matcher.match(old, current)

        then:
        events.size() == 1
        with(events[0]) {
            type == DomainEventType.BETOFFER_STATUS_CHANGE
            payload == "ko"
        }
    }

    def "should fallback when BetOffer name changes"() {
        given:
        def old = defaultBetOffer()
        def current = defaultBetOffer()
        current.betOffer.name = "changed"

        when:
        def events = matcher.match(old, current)

        then:
        events.size() == 1
        events[0].type == DomainEventType.BETOFFER_FULL_UPDATE
    }

    def "should ignore when BetOffer date changes"() {
        given:
        def old = defaultBetOffer()
        def current = defaultBetOffer()
        current.betOffer.date = Optional.of("changed")


        when:
        def events = matcher.match(old, current)

        then:
        events.size() == 0
    }

    def "should ignore when BetOffer tags reordered"() {
        given:
        def old = defaultBetOffer()
        def current = defaultBetOffer()
        current.betOffer.tags = ["t2", "t1"]


        when:
        def events = matcher.match(old, current)

        then:
        events.size() == 0
    }

    def "should detect when BetOffer tag was added and generate full betoffer update"() {
        given:
        def old = defaultBetOffer()
        def current = defaultBetOffer()
        current.betOffer.tags = ["t2", "t1", "t3"]


        when:
        def events = matcher.match(old, current)

        then:
        events.size() == 1
        with(events[0]) {
            type == DomainEventType.BETOFFER_FULL_UPDATE
            payload.betOffer.tags == ["t2", "t1", "t3"]
        }
    }

    def "should detect when BetOffer tag was remove and generate full betoffer update"() {
        given:
        def old = defaultBetOffer()
        def current = defaultBetOffer()
        current.betOffer.tags = ["t2"]


        when:
        def events = matcher.match(old, current)

        then:
        events.size() == 1
        with(events[0]) {
            type == DomainEventType.BETOFFER_FULL_UPDATE
            payload.betOffer.tags == ["t2"]
        }
    }

    def "should detect odds changes"() {
        given:
        def old = defaultBetOffer()
        def current = defaultBetOffer()
        current.betOffer.outcomes[0].odds = Optional.of(new BigDecimal("3.45"))

        when:
        def events = matcher.match(old, current)

        then:
        events.size() == 1
        with(events[0]) {
            type == DomainEventType.OUTCOME_ODDS_CHANGE
            payload == Optional.of(new BigDecimal("3.45"))
        }

    }

    def "should detect outcome status and cashOutStatus changes"() {
        given:
        def old = defaultBetOffer()
        def current = defaultBetOffer()
        current.betOffer.outcomes[0].status = "newStatus"
        current.betOffer.outcomes[0].cashOutStatus = "newCashOutStatus"

        when:
        def events = matcher.match(old, current)

        then:
        events*.type.sort() == [DomainEventType.OUTCOME_STATUS_CHANGE, DomainEventType.OUTCOME_CASHOUT_STATUS_CHANGE]

    }

    def "should detect probs changes"() {
        given:
        def old = defaultBetOffer()
        def current = defaultBetOffer()
        current.betOffer.outcomes[0].probabilities.p1 = OptionalDouble.of(3.45d)

        when:
        def events = matcher.match(old, current)

        then:
        events.size() == 1
        with(events[0]) {
            type == DomainEventType.OUTCOME_PROBABILITIES_CHANGE
            payload.p1 == OptionalDouble.of(3.45d)
        }

    }

    def "should ignore Outcome updateTm  changes"() {
        given:
        def old = defaultBetOffer()
        def current = defaultBetOffer()
        current.betOffer.outcomes[0].updatedTm = 2

        when:
        def events = matcher.match(old, current)

        then:
        events.size() == 0

    }

    def "should handle bigdecimal precision"() {
        given:
        def old = defaultBetOffer()
        def current = defaultBetOffer()
        current.betOffer.outcomes[0].odds = Optional.of(new BigDecimal("1.230001"))

        when:
        def events = matcher.match(old, current)

        then:
        events.size() == 0

    }

    BulkedBetOffer defaultBetOffer() {
        def probabilities = new Probabilities(OptionalDouble.of(0.5d))
        def outcomes = [new Outcome(123, "status", "cashOutStatus", Optional.of(new BigDecimal("1.23")), 9999, probabilities)]
        return new BulkedBetOffer(new BetOffer(1, "ok", "name", "date", ["t1", "t2"], outcomes), [1, 2] as Set, [1, 2] as Set);
    }
}
