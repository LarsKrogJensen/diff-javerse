package domain;

import java.util.Set;

public class BulkedBetOffer {
    public final BetOffer betOffer;
    public final Set<Integer> liveOfferings;
    public final Set<Integer> prematchOfferings;

    public BulkedBetOffer(BetOffer betOffer, Set<Integer> liveOfferings, Set<Integer> prematchOfferings) {
        this.betOffer = betOffer;
        this.liveOfferings = liveOfferings;
        this.prematchOfferings = prematchOfferings;
    }
}
