package domain;


import java.math.BigDecimal;
import java.util.Optional;

public class Outcome {
    public long id;
    public String status;
    public String cashOutStatus;
    public Optional<BigDecimal> odds;
    public long updatedTm;
    public Probabilities probabilities;
//    public BigDecimal someValue = new BigDecimal("10.567");

    public Outcome(long id, String status, String cashOutStatus, Optional<BigDecimal> odds, long updatedTm, Probabilities probabilities) {
        this.id = id;
        this.status = status;
        this.cashOutStatus = cashOutStatus;
        this.odds = odds;
        this.updatedTm = updatedTm;
        this.probabilities = probabilities;
    }

}
