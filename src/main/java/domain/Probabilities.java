package domain;

import java.util.OptionalDouble;

public class Probabilities {
    public OptionalDouble p1;
    public OptionalDouble p2;
    public OptionalDouble p3;

    public Probabilities(OptionalDouble p1, OptionalDouble p2, OptionalDouble p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public Probabilities(OptionalDouble p1) {
        this(p1,p1,p1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Probabilities that = (Probabilities) o;

        if (!p1.equals(that.p1)) return false;
        if (!p2.equals(that.p2)) return false;
        return p3.equals(that.p3);
    }

    @Override
    public int hashCode() {
        int result = p1.hashCode();
        result = 31 * result + p2.hashCode();
        result = 31 * result + p3.hashCode();
        return result;
    }
}
