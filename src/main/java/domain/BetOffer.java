package domain;

import java.util.List;
import java.util.Optional;

public class BetOffer {
    public long id;
    public String status;
    public String name;
    public Optional<String> date;
    public List<String> tags;
    public List<Outcome> outcomes;

    public BetOffer(long id, String status, String name, String date, List<String> tags, List<Outcome> outcomes) {
        this.id = id;
        this.status = status;
        this.name = name;
        this.date = Optional.ofNullable(date);
        this.tags = tags;
        this.outcomes = outcomes;
    }
}
