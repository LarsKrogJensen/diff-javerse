package delta;

import events.DomainEvent;
import org.javers.core.diff.Change;
import org.javers.core.diff.changetype.ValueChange;

import java.util.Objects;
import java.util.function.BiFunction;

class PropertyMatcher<T> implements Matcher{
    private final Class<T> type;
    private final String property;
    private BiFunction<DeltaContext, T, DomainEvent> converter;

    PropertyMatcher(Class<T> type, String property) {
        this.type = type;
        this.property = property;
    }

    PropertyMatcher(Class<T> type, String property, BiFunction<DeltaContext, T, DomainEvent> converter) {
        this.type = type;
        this.property = property;
        this.converter = converter;
    }

    @Override
    public boolean matches(Change change) {
        if (change instanceof ValueChange) {
            ValueChange valueChange = (ValueChange) change;
            return valueChange.getPropertyName().equalsIgnoreCase(property) &&
                        type.equals(change.getAffectedObject().map(Object::getClass).orElse(null));
        }

        return false;
    }

    @Override
    public DomainEvent convert(DeltaContext context, Change change) {
        Objects.requireNonNull(converter);
        ValueChange valueChange = (ValueChange) change;
        //noinspection unchecked
        return converter.apply(context, (T) valueChange.getAffectedObject().get());
    }
}
