package delta;

import events.DomainEvent;
import org.javers.common.string.PrettyValuePrinter;
import org.javers.core.Javers;
import org.javers.core.diff.Change;
import org.javers.core.diff.Diff;
import org.javers.core.diff.changetype.ValueChange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.LongSupplier;

import static java.math.RoundingMode.HALF_UP;
import static java.util.stream.Collectors.toList;

public class DeltaEngine<T> {
    private static final Logger log = LoggerFactory.getLogger(DeltaEngine.class);
    private final List<Matcher> matchers = new ArrayList<>();
    private final List<Matcher> ignored = new ArrayList<>();
    private final LongSupplier sequenceGenerator;
    private Function<DeltaContext<T>, DomainEvent> fallback;

    private final Javers diffTool;

    public DeltaEngine(Javers diffTool, LongSupplier sequenceGenerator) {
        this.diffTool = diffTool;
        this.sequenceGenerator = sequenceGenerator;
    }

    public <R> void registerProperty(Class<R> type, String property, BiFunction<DeltaContext<T>, R, DomainEvent> converter) {
        //noinspection unchecked
        matchers.add(new PropertyMatcher(type, property, converter));
    }

    public void ignoreProperty(Class<?> type, String field) {
        ignored.add(new PropertyMatcher<>(type, field));
    }

    public void fallback(Function<DeltaContext<T>, DomainEvent> fallback) {
        this.fallback = fallback;
    }

    public List<DomainEvent> match(T old, T current) {
        List<DomainEvent> events = new ArrayList<>();
        Diff diff = diffTool.compare(old, current);

        DeltaContext<T> context = new DeltaContext<>(old, current, sequenceGenerator);

        try {
            for (Change change : sanitize(diff).getChanges()) {
                Optional<Matcher> matcher = findMatcher(change);
                if (matcher.isPresent()) {
                    //noinspection unchecked
                    events.add(matcher.get().convert(context, change));
                } else if (!shouldIgnore(change)) {
                    return List.of(fallback.apply(context));
                }
            }
        } catch (Exception e) {
            log.error("Failed to match objects", e);
            return List.of(fallback.apply(context));
        }

        return events;
    }

    private Diff sanitize(Diff diff) {
        if (!diff.hasChanges()) {
            return diff;
        }
        List<Change> changes = diff.getChanges().stream().filter(chg -> sanitizeBigDecimalPredicate(chg, 4)).collect(toList());
        if (changes.size() == diff.getChanges().size()) {
            return diff;
        }

        return newDiff(changes);
    }

    private boolean sanitizeBigDecimalPredicate(Change change, int scale) {
        if (change instanceof ValueChange) {
            ValueChange vc = (ValueChange) change;

            Optional<BigDecimal> left = tryConvertToBigDecimal(vc.getLeft());
            Optional<BigDecimal> right = tryConvertToBigDecimal(vc.getRight());

            if (left.isPresent() && right.isPresent()) {
                BigDecimal leftRounded = left.get().setScale(scale, HALF_UP);
                BigDecimal rightRounded = right.get().setScale(scale, HALF_UP);

                return !leftRounded.equals(rightRounded);
            }
        }
        return true;
    }

    private Optional<BigDecimal> tryConvertToBigDecimal(Object obj) {
        if (obj instanceof Optional && ((Optional) obj).isPresent() && ((Optional) obj).get() instanceof BigDecimal) {
            //noinspection unchecked
            return ((Optional<BigDecimal>) obj);
        }
        return Optional.empty();
    }

    private Diff newDiff(List<Change> changes) {
        try {
            Constructor<Diff> constructor = Diff.class.getDeclaredConstructor(List.class, PrettyValuePrinter.class);
            constructor.setAccessible(true);
            return constructor.newInstance(changes, PrettyValuePrinter.getDefault());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Optional<Matcher> findMatcher(Change change) {
        return matchers.stream().filter(m -> m.matches(change)).findFirst();
    }

    private boolean shouldIgnore(Change change) {
        return ignored.stream().anyMatch(m -> m.matches(change));
    }

}
