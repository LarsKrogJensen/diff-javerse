package delta;

import events.DomainEvent;
import org.javers.core.diff.Change;

interface Matcher<T> {
    boolean matches(Change change);
    DomainEvent convert(DeltaContext<T> context, Change change);
}
