package delta;

import java.util.function.LongSupplier;

public class DeltaContext<T> {
    public final T oldSource;
    public final T currentSource;
    public final LongSupplier sequenceGenerator;

    public DeltaContext(T oldSource, T currentSource, LongSupplier sequenceGenerator) {
        this.oldSource = oldSource;
        this.currentSource = currentSource;
        this.sequenceGenerator = sequenceGenerator;
    }
}
