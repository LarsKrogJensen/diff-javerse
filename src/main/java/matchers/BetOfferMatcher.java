package matchers;

import delta.DeltaContext;
import delta.DeltaEngine;
import domain.BetOffer;
import domain.BulkedBetOffer;
import domain.Outcome;
import domain.Probabilities;
import events.DomainEvent;
import events.DomainEventType;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.metamodel.clazz.EntityDefinition;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.function.LongSupplier;

import static org.javers.core.diff.ListCompareAlgorithm.AS_SET;

public class BetOfferMatcher  {
    private DeltaEngine<BulkedBetOffer> deltaEngine;

    public BetOfferMatcher(LongSupplier sequenceGenerator) {
        Javers javers = JaversBuilder.javers()
                .registerValue(OptionalDouble.class)
                .registerValue(OptionalInt.class)
//                .registerValue(BigDecimal.class)
//                .registerValue(Optional.class)
//                .registerType()
//                .registerCustomComparator(new CustomOptionalComparator(2), Optional.class)
//                .registerCustomComparator(new CustomBigDecimalComparator(2), BigDecimal.class)
//                .registerValueWithCustomToString(BigDecimal.class, this::bigDecimalToString)
                .registerValue(Probabilities.class) // should consider this a a value in order to have the Outcome.propbabilities to detect change
                .registerEntity(new EntityDefinition(Outcome.class, "id")) // identifier to determine if entities are same
                .registerEntity(new EntityDefinition(BetOffer.class, "id")) // identifier to determine if entities are same
                .withListCompareAlgorithm(AS_SET) // avoid reordering triggers a change
                .build();
        deltaEngine = new DeltaEngine<>(javers, sequenceGenerator);

        deltaEngine.registerProperty(BetOffer.class, "status", this::betOfferStatusChanged);
        deltaEngine.registerProperty(Outcome.class, "cashOutStatus", this::outcomeCashOutStatusChanged);
        deltaEngine.registerProperty(Outcome.class, "status", this::outcomeStatusChanged);
        deltaEngine.registerProperty(Outcome.class, "odds", this::outcomeOddsChanged);
        deltaEngine.registerProperty(Outcome.class, "probabilities", this::outcomeProbsChanged);


        deltaEngine.ignoreProperty(Outcome.class, "updatedTm");
        deltaEngine.ignoreProperty(BetOffer.class, "date");

        deltaEngine.fallback(this::fallback);
    }

    private  String bigDecimalToString(BigDecimal decimal) {
        return decimal.toString();
    }

    public List<DomainEvent> match(BulkedBetOffer old, BulkedBetOffer current) {
        Objects.requireNonNull(old);
        Objects.requireNonNull(current);
        return deltaEngine.match(old, current);
    }

    private DomainEvent outcomeProbsChanged(DeltaContext<BulkedBetOffer> context, Outcome newOutcome) {
        return new DomainEvent(DomainEventType.OUTCOME_PROBABILITIES_CHANGE, newOutcome.probabilities, context.sequenceGenerator.getAsLong());
    }

    private DomainEvent fallback(DeltaContext<BulkedBetOffer> context) {
        return new DomainEvent(DomainEventType.BETOFFER_FULL_UPDATE, context.currentSource, context.sequenceGenerator.getAsLong());
    }

    private  DomainEvent betOfferStatusChanged(DeltaContext<BulkedBetOffer> context, BetOffer newBetoffer) {
        return new DomainEvent(DomainEventType.BETOFFER_STATUS_CHANGE, newBetoffer.status, context.sequenceGenerator.getAsLong());
    }

    private DomainEvent outcomeOddsChanged(DeltaContext<BulkedBetOffer> context, Outcome newOutcome) {
        return new DomainEvent(DomainEventType.OUTCOME_ODDS_CHANGE, newOutcome.odds, context.sequenceGenerator.getAsLong());
    }

    private DomainEvent outcomeStatusChanged(DeltaContext<BulkedBetOffer> context, Outcome newOutcome) {
        return new DomainEvent(DomainEventType.OUTCOME_STATUS_CHANGE, newOutcome.status, context.sequenceGenerator.getAsLong());
    }

    private DomainEvent outcomeCashOutStatusChanged(DeltaContext<BulkedBetOffer> context, Outcome newOutcome) {
        return new DomainEvent(DomainEventType.OUTCOME_CASHOUT_STATUS_CHANGE, newOutcome.status, context.sequenceGenerator.getAsLong());
    }
}
