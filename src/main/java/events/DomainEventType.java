package events;

public enum DomainEventType {
    OUTCOME_ODDS_CHANGE,
    OUTCOME_PROBABILITIES_CHANGE,
    OUTCOME_STATUS_CHANGE,
    OUTCOME_CASHOUT_STATUS_CHANGE,

    BETOFFER_ADDED,
    BETOFFER_STATUS_CHANGE,
    BETOFFER_CASHOUT_STATUS_CHANGE,
    BETOFFER_FULL_UPDATE,
    BETOFFER_DELETED,

    EVENT_ADDED,
    EVENT_DELETED,
}
