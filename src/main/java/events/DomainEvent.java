package events;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

public class DomainEvent {
    public final Object payload;
    public final long timestamp;
    public final DomainEventType type;
    public final Optional<Set<Integer>> offerings;

    public DomainEvent(DomainEventType type, Object payload, long timestamp) {
        this.type = type;
        this.payload = payload;
        this.timestamp = timestamp;
        this.offerings = Optional.empty();
    }

    public DomainEvent(DomainEventType type, Object payload, long timestamp, Optional<Set<Integer>> offerings) {
        this.type = type;
        this.payload = payload;
        this.timestamp = timestamp;
        this.offerings = offerings.map(Collections::unmodifiableSet);
    }

    @SuppressWarnings("unchecked")
    public <T> T payload() {
        return (T) payload;
    }


    public DomainEvent withOfferings(Set<Integer> offeringIds) {
        if (offerings.isPresent()) {
            throw new IllegalStateException(String.format("Trying to set offerings %s to DomainEvent that already has offerings %s", offeringIds, this.offerings.get()));
        }
        return new DomainEvent(this.type, this.payload, this.timestamp, Optional.of(offeringIds));
    }
}
